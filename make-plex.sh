make(){
    fontforge -lang py -script ligaturize.py fonts/plex/IBM-Plex-Mono/fonts/complete/otf/$1 --output-dir=fonts/output/plex-mono/ --output-name='Plex Mono' --prefix='' &
}

make IBMPlexMono-Bold.otf
make IBMPlexMono-BoldItalic.otf
make IBMPlexMono-ExtraLight.otf
make IBMPlexMono-ExtraLightItalic.otf
make IBMPlexMono-Italic.otf
make IBMPlexMono-Light.otf
make IBMPlexMono-LightItalic.otf
make IBMPlexMono-Medium.otf
make IBMPlexMono-MediumItalic.otf
make IBMPlexMono-Regular.otf
make IBMPlexMono-SemiBold.otf
make IBMPlexMono-SemiBoldItalic.otf
make IBMPlexMono-Text.otf
make IBMPlexMono-TextItalic.otf
make IBMPlexMono-Thin.otf
make IBMPlexMono-ThinItalic.otf

