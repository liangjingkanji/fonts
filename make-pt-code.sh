make(){
    fontforge -lang py -script ligaturize.py fonts/output/pt-mono/$1 --output-dir=fonts/output/pt-mono/ --output-name='PT Code' --prefix='' &
}

make PTM55F.ttf
make PTM75F.ttf
